#include "gtk/gtkcssprovider.h"
#include "pango/pango-attributes.h"
#include "pango/pango-font.h"
#include <gtk-layer-shell/gtk-layer-shell.h>
#include <gtk/gtk.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define CLICK_INTERVAL 400
#define LONG_TAP_INTERVAL 1000
#define MIN_DISTANCE 50
#define HORIZONTAL_SECTIONS 5
#define MAX_WORKSPACES 10

struct point
{
  int32_t x, y;
};

struct app_state
{
  bool placement_top;
  char *font_string;
  double opacity;
  char *background_color;

  enum
  {
    CLICK_IDLE,
    CLICK_SINGLE,
    CLICK_DOUBLE,
    CLICK_TRIPLE
  } click_state;

  enum
  {
    TOUCH_IDLE,
    TOUCH_SINGLE,
    TOUCH_SINGLE_IDLE,
    TOUCH_DOUBLE,
    TOUCH_DOUBLE_IDLE,
    TOUCH_TRIPLE,
    TOUCH_LONG,
    TOUCH_SWIPE_HORIZONTAL,
    TOUCH_SWIPE_VERTICAL
  } touch_state;

  struct point initial_point;
  uint32_t initial_time;

  GString *labels[3];
  GtkWidget *main_left, *main_center, *main_right;

  GString *custom_commands[12];

  GString *switch_workspace_cmd,
      *next_window_cmd, *prev_window_cmd;

  GString *current_workspace_cmd;
  uint8_t current_workspace;

  uint8_t current_section, touch_zone;
  uint32_t window_width, window_height, horizontal_section_length, vertical_section_length;

  char *status_label;

  bool apps_hidden;
};

int32_t
abs (int32_t val)
{
  return (val > 0 ? val : -val);
}

void
calculate_zone_and_sections (struct app_state *state)
{
  GtkWidget *win = gtk_widget_get_parent (state->main_left);
  state->window_width = gtk_widget_get_allocated_width (win);
  state->window_height = gtk_widget_get_allocated_height (win);
  state->horizontal_section_length = state->window_width / HORIZONTAL_SECTIONS;
  state->vertical_section_length = state->window_height;
  int32_t zone_width = state->window_width / 3;
  if (state->initial_point.x < zone_width)
    {
      state->touch_zone = 0; // left
    }
  else if (state->initial_point.x > 2 * zone_width)
    {
      state->touch_zone = 2; // right
    }
  else
    {
      state->touch_zone = 1; // middle
    }
}

uint8_t
get_horizontal_section (struct app_state *state, int32_t x)
{
  return x / state->horizontal_section_length;
}

void
restore_labels (struct app_state *state)
{
  gtk_label_set_markup (GTK_LABEL (state->main_left), g_strdup (state->labels[0]->str));
  gtk_label_set_markup (GTK_LABEL (state->main_center), g_strdup (state->labels[1]->str));
  gtk_label_set_markup (GTK_LABEL (state->main_right), g_strdup (state->labels[2]->str));
}

uint8_t
get_current_workspace (GString *command)
{
  /*nn  FILE *fp = popen (command->str, "r");
  char num[3];
  fgets (num, 3, fp);
  pclose (fp);
  return atoi (num);*/
  (void) command;
  return 3;
}

void
show_desktop (bool apps_hidden, int current_workspace, char *status_label)
{
  if (!apps_hidden)
    {
      system ("swaymsg workspace 255");
    }
  else
    {
      g_snprintf (status_label, 200, "swaymsg workspace %d", current_workspace);
      system (status_label);
    }
}

void
process_swipe (struct app_state *state, int32_t x, int32_t y)
{
  (void) y;
  uint8_t current_section = get_horizontal_section (state, x);
  if (current_section != state->current_section)
    {
      switch (state->touch_zone)
        {
        case 2:
        case 0:
          if (state->current_section > current_section)
            {
              state->current_workspace = (state->current_workspace == 1)
                                             ? 1
                                             : state->current_workspace - 1;
            }
          else
            {
              state->current_workspace = (state->current_workspace == MAX_WORKSPACES)
                                             ? MAX_WORKSPACES
                                             : state->current_workspace + 1;
            }

          g_snprintf (state->status_label, 200, state->switch_workspace_cmd->str, state->current_workspace);
          system (state->status_label);
          g_snprintf (state->status_label, 200, "Workspace %d", state->current_workspace);
          break;
        case 1:
          if (state->current_section > current_section)
            {
              system (state->prev_window_cmd->str);
            }
          else
            {
              system (state->next_window_cmd->str);
            }
          break;
        }
      state->current_section = current_section;
      gtk_label_set_markup (GTK_LABEL (state->main_center), state->status_label);
    }
}

void
process_custom_command (struct app_state *state)
{
  uint8_t command = 0;
  switch (state->touch_state)
    {
    case TOUCH_SINGLE_IDLE:
      command = 0 + state->touch_zone;
      break;
    case TOUCH_DOUBLE_IDLE:
      command = 3 + state->touch_zone;
      break;
    case TOUCH_TRIPLE:
      command = 6 + state->touch_zone;
      break;
    case TOUCH_LONG:
      command = 9 + state->touch_zone;
      break;
    default:
      break;
    }

  if (command == 1)
    { // show desktop crutch

      show_desktop (state->apps_hidden, state->current_workspace, state->status_label);
      state->apps_hidden = !state->apps_hidden;
    }
  else
    {
      system (state->custom_commands[command]->str);
    }

  switch (state->click_state)
    {
    case CLICK_SINGLE:
      break;
    case CLICK_DOUBLE:
      break;
    case CLICK_TRIPLE:
      break;
    default:
      break;
    }
}

static gboolean
timer_click_release (gpointer user_data)
{
  struct app_state *state = user_data;
  switch (state->click_state)
    {
    case CLICK_IDLE:
    case CLICK_TRIPLE:
      break;
    case CLICK_SINGLE:
    case CLICK_DOUBLE:
      process_custom_command (state);
      state->click_state = CLICK_IDLE;
      break;
    }

  return FALSE;
}

static gboolean
timer_touch_release (gpointer user_data)
{
  struct app_state *state = user_data;

  switch (state->touch_state)
    {
    case TOUCH_SINGLE_IDLE:
    case TOUCH_DOUBLE_IDLE:
      process_custom_command (state);
      state->touch_state = TOUCH_IDLE;
    default:
      break;
    }

  return FALSE;
}

static gboolean
click_event (GtkWidget *widget, GdkEventButton *event, gpointer user_data)
{

  (void) widget;

  struct app_state *state = user_data;

  if (event->type == GDK_BUTTON_PRESS)
    {
      state->click_state = CLICK_SINGLE;
      g_timeout_add (CLICK_INTERVAL, G_SOURCE_FUNC (timer_click_release), state);
    }
  else if (event->type == GDK_DOUBLE_BUTTON_PRESS)
    {
      state->click_state = CLICK_DOUBLE;
    }
  else if (event->type == GDK_TRIPLE_BUTTON_PRESS)
    {
      state->click_state = CLICK_TRIPLE;
      process_custom_command (state);
    }
  return TRUE;
}

static gboolean
touch_event (GtkWidget *widget, GdkEventTouch *event, gpointer user_data)
{
  (void) widget;

  struct app_state *state = user_data;

  switch (event->type)
    {
    case GDK_TOUCH_BEGIN:
      if (state->touch_state == TOUCH_IDLE || state->touch_state == TOUCH_SINGLE_IDLE || state->touch_state == TOUCH_DOUBLE_IDLE)
        {
          state->initial_time = event->time;
          state->initial_point.x = event->x;
          state->initial_point.y = event->y;
          calculate_zone_and_sections (state);
          switch (state->touch_state)
            {
            case TOUCH_IDLE:
              state->touch_state = TOUCH_SINGLE;
              g_timeout_add (CLICK_INTERVAL, G_SOURCE_FUNC (timer_touch_release), state);
              break;
            case TOUCH_SINGLE_IDLE:
              state->touch_state = TOUCH_DOUBLE;
              break;
            case TOUCH_DOUBLE_IDLE:
              state->touch_state = TOUCH_TRIPLE;
              break;
            default:
              break;
            }
        }
      break;
    case GDK_TOUCH_UPDATE:
      if (state->touch_state == TOUCH_SINGLE)
        {
          int32_t ev_x = (int32_t) event->x;
          int32_t ev_y = (int32_t) event->y;
          int32_t dist_x = abs (ev_x - state->initial_point.x);
          int32_t dist_y = abs (ev_y - state->initial_point.y);
          if (dist_x > MIN_DISTANCE || dist_y > MIN_DISTANCE)
            {
              gtk_label_set_markup (GTK_LABEL (state->main_left), "");
              gtk_label_set_markup (GTK_LABEL (state->main_center), "");
              gtk_label_set_markup (GTK_LABEL (state->main_right), "");
              if (dist_x > dist_y)
                {
                  state->touch_state = TOUCH_SWIPE_HORIZONTAL;
                  state->current_section = get_horizontal_section (state, state->initial_point.x);
                  state->current_workspace = get_current_workspace (state->current_workspace_cmd);
                  process_swipe (state, (int32_t) event->x, (int32_t) event->y);
                }
              else
                {
                  state->touch_state = TOUCH_SWIPE_VERTICAL;
                }
            }
        }
      else if (state->touch_state == TOUCH_SWIPE_VERTICAL || state->touch_state == TOUCH_SWIPE_HORIZONTAL)
        {
          process_swipe (state, (int32_t) event->x, (int32_t) event->y);
        }
      break;
    case GDK_TOUCH_END:
    case GDK_TOUCH_CANCEL:
      switch (state->touch_state)
        {
        case TOUCH_SINGLE:
          if (event->time - state->initial_time >= LONG_TAP_INTERVAL)
            {
              state->touch_state = TOUCH_LONG;
              process_custom_command (state);
              state->touch_state = TOUCH_IDLE;
            }
          else
            {
              state->touch_state = TOUCH_SINGLE_IDLE;
            }
          break;
        case TOUCH_DOUBLE:
          state->touch_state = TOUCH_DOUBLE_IDLE;
          break;
        case TOUCH_TRIPLE:
          process_custom_command (state);
          state->touch_state = TOUCH_IDLE;
          break;
        case TOUCH_SWIPE_VERTICAL:
          // TODO process function
          g_print ("Vertical Swipe Event\n");
          restore_labels (state);
          state->touch_state = TOUCH_IDLE;
          break;
        case TOUCH_SWIPE_HORIZONTAL:
          restore_labels (state);
          state->touch_state = TOUCH_IDLE;
        default:
          break;
        }
      break;
    default:
      break;
    }

  return TRUE;
}

static void
app_activate (GtkApplication *app, void *data)
{
  struct app_state *state = data;
  GtkWidget *win = gtk_application_window_new (app);
  gtk_widget_set_opacity (win, state->opacity);

  GtkCssProvider *provider = gtk_css_provider_new ();
  GString *css = g_string_new ("");
  g_snprintf (css->str, 100, "window {background-color: %s};", state->background_color);
  gtk_css_provider_load_from_data (provider, css->str, -1, NULL);
  GtkStyleContext *context = gtk_widget_get_style_context (win);
  gtk_style_context_add_provider (context, GTK_STYLE_PROVIDER (provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

  g_signal_connect (win, "button-press-event", G_CALLBACK (click_event), state);
  gtk_widget_add_events (win, GDK_TOUCH_MASK);
  g_signal_connect (win, "touch-event", G_CALLBACK (touch_event), state);

  gtk_layer_init_for_window (GTK_WINDOW (win));
  gtk_layer_set_layer (GTK_WINDOW (win), GTK_LAYER_SHELL_LAYER_TOP);
  gtk_layer_auto_exclusive_zone_enable (GTK_WINDOW (win));

  gtk_layer_set_anchor (GTK_WINDOW (win), GTK_LAYER_SHELL_EDGE_LEFT, true);
  gtk_layer_set_anchor (GTK_WINDOW (win), GTK_LAYER_SHELL_EDGE_RIGHT, true);
  GtkLayerShellEdge edge = (state->placement_top) ? (GTK_LAYER_SHELL_EDGE_TOP) : (GTK_LAYER_SHELL_EDGE_BOTTOM);
  gtk_layer_set_anchor (GTK_WINDOW (win), edge, true);

  PangoFontDescription *font_desc =
      pango_font_description_from_string (state->font_string);
  PangoAttribute *attr =
      pango_attr_font_desc_new (font_desc);
  pango_font_description_free (font_desc);
  attr->start_index = 0;
  attr->end_index = 255;
  PangoAttrList *attr_list = pango_attr_list_new ();
  pango_attr_list_insert (attr_list, attr);

  GtkWidget *grid = gtk_grid_new ();
  gtk_grid_set_column_homogeneous (GTK_GRID (grid), TRUE);
  gtk_container_add (GTK_CONTAINER (win), grid);

  state->main_left = gtk_label_new ("");
  gtk_label_set_attributes (GTK_LABEL (state->main_left), attr_list);
  gtk_label_set_justify (GTK_LABEL (state->main_left), GTK_JUSTIFY_LEFT);
  gtk_widget_set_halign (state->main_left, GTK_ALIGN_START);

  gtk_grid_attach (GTK_GRID (grid), state->main_left, 0, 0, 1, 1);

  state->main_center = gtk_label_new ("");
  gtk_label_set_attributes (GTK_LABEL (state->main_center), attr_list);
  gtk_label_set_justify (GTK_LABEL (state->main_center), GTK_JUSTIFY_CENTER);
  gtk_grid_attach (GTK_GRID (grid), state->main_center, 1, 0, 1, 1);

  state->main_right = gtk_label_new ("");
  gtk_label_set_attributes (GTK_LABEL (state->main_right), attr_list);
  gtk_label_set_justify (GTK_LABEL (state->main_right), GTK_JUSTIFY_RIGHT);
  gtk_widget_set_halign (state->main_right, GTK_ALIGN_END);
  gtk_grid_attach (GTK_GRID (grid), state->main_right, 2, 0, 1, 1);

  restore_labels (state);

  gtk_widget_show_all (GTK_WIDGET (win));
}

void
init_values (struct app_state *state)
{
  state->font_string = "Liberation Mono 25";
  state->placement_top = false;
  state->opacity = 0.5;
  state->background_color = "black";

  state->touch_state = TOUCH_IDLE;
  state->click_state = CLICK_IDLE;

  int i = 0;
  // single tap left/middle/right
  state->custom_commands[i++] = g_string_new ("echo \"Left Zone Single Tap\"");
  state->custom_commands[i++] = g_string_new ("swaymsg ");
  state->custom_commands[i++] = g_string_new ("keyboard-toggle");
  // double tap left/middle/right
  state->custom_commands[i++] = g_string_new ("echo \"Left Zone Double Tap\"");
  state->custom_commands[i++] = g_string_new ("wofi --show run,drun,dmenu -K");
  state->custom_commands[i++] = g_string_new ("echo \"Right Zone Double Tap\"");
  // triple tap left mid right
  state->custom_commands[i++] = g_string_new ("echo \"Left Zone Triple Tap\"");
  state->custom_commands[i++] = g_string_new ("swaymsg kill");
  state->custom_commands[i++] = g_string_new ("echo \"Right Zone Triple Tap\"");
  // long tap left/mid/right
  state->custom_commands[i++] = g_string_new ("echo \"Left Zone Long Tap\"");
  state->custom_commands[i++] = g_string_new ("echo \"Mid Zone Long Tap\"");
  state->custom_commands[i++] = g_string_new ("echo \"Right Zone Long Tap\"");

  i = 0;
  state->labels[i++] = g_string_new ("BT WF MB");
  state->labels[i++] = g_string_new ("20:48");
  state->labels[i++] = g_string_new ("51%");

  state->next_window_cmd = g_string_new ("swaymsg focus next");
  state->prev_window_cmd = g_string_new ("swaymsg focus prev");
  state->switch_workspace_cmd = g_string_new ("swaymsg workspace %d");

  state->status_label = calloc (sizeof (char), 200);

  state->current_workspace_cmd = g_string_new ("swaymsg -t get_workspaces | grep -B 3 \"focused.*true\" | grep num | cut -d: -f2 | tr -d ,[:blank:]");
  state->current_workspace = get_current_workspace (state->current_workspace_cmd);

  state->apps_hidden = false;
}

int
main (void)
{
  struct app_state state = { 0 };
  init_values (&state);
  GtkApplication *app = gtk_application_new ("org.scg.rubick", G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "activate", G_CALLBACK (app_activate), &state);
  int status = g_application_run (G_APPLICATION (app), 0, NULL);
  g_object_unref (app);
  return status;
}
