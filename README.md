TODO

* [x] Basic Gtk3 + gtk-layer-shell
* Draw a base panel widget with the info
  * clock
  * date
  * wifi
  * bt
  * mobile
  * battery
  * customizable
      * [x] 3 labels with a script for each
          * [x] Insert 3 labels 
          * [x] Add click/tap events
              * [x] single
                  * [x] Timer for single click detection
              * [x] double
              * [x] triple
          * Add executing text info scripts on timer
              * GIO listening to IPC sockets
          * different timers
* Show on all or selected output
* [x] config placement: up/down
* custom theming
    * [x] transparency
    * [x] font face
    * [x] font size
    * [ ] text color
        * [ ] Pango markup
    * [x] background color
* [x] touch: 3 zones for curtain + launch menu + hiding apps, swiping left+right for switching windows
    * [x] Enable touch events
    * [x] state machine for touch events
        * [x] tap
        * [x] double tap
        * [x] triple tap
        * [x] swipe up
        * [x] swipe left/right
        * [x] long tap
    * [x] 3 zones;
* Functionality:
    * [x] Switch windows
        * [x] Continuous Swipe left/right from middle
            * [x] store/restore labels
            * [x] separate calculation of the current touch zone and swipe sections
            * [x] Calculate current and initial sections
            * [x] launch commands
                * [x] next/prev window from center
                * [x] next/prev workspace from left
                * [x] prev/next workspace from right
                * [x] add commands to state
    * [x] Switch workspaces
        * [x] Continuous Swipe left/right from left/right sections
        * [x] Show workspace number on the panel
            * [x] fetch current workspace
            * [x] move to numbered workspaces, not just existing ones
        * [x] Add commmand to state
    * [x] Show desktop
        * [x] separate desktop function
    * Close app
        * [x] close command
        * Show close icon
            * Show custom icons on double/triple/long taps
                * Add custom unicode icons to actions config
            * Show icon when switching to single/douoble/triple/long tap
    * Move next/previous workspace
        * Continuos Swipe up/down from left/right to get the workspace number, execute on touch up
    * move windows with vertical swipe?
    * [x] Custom actions on tap
        * [x] string storage
        * [x] processing function
            * [x] launch commands
        * [x] get window dimensions
* [ ] config file
    * [ ] special marker for specifying built-in commands like show desktop
* [ ] sway ipc
    * Show window titles on the panel
    * Subscribe to workspace changes
* [ ] other compositors ipc?
* [ ] tray
